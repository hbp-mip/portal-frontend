import { useReactiveVar } from '@apollo/client';
import * as d3 from 'd3';
import { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import styled from 'styled-components';
import { draftExperimentVar, selectedDomainVar } from '../API/GraphQL/cache';
import { HierarchyCircularNode } from '../utils';
import CirclePack from './D3CirclePackLayer';
import { d3Hierarchy, groupsToTreeView, NodeData } from './d3Hierarchy';

const diameter = 800;
const padding = 1.5;

export interface Props {
  selectedNode: HierarchyCircularNode | undefined;
  handleSelectNode: (node: HierarchyCircularNode) => void;
}

const SpinnerContainer = styled.div`
  display: flex;
  min-height: inherit;
  justify-content: center;
  align-items: center;
`;

const D3Container = ({ selectedNode, handleSelectNode }: Props) => {
  const domain = useReactiveVar(selectedDomainVar);
  const draftExp = useReactiveVar(draftExperimentVar);
  const datasets = draftExp?.datasets;

  const [d3Layout, setD3Layout] = useState<HierarchyCircularNode>();

  useEffect(() => {
    if (!domain) return;

    //Build group tree with variables
    const rootNode = groupsToTreeView(
      domain.rootGroup,
      domain.groups,
      domain.variables,
      datasets
    );

    const hierarchyNode = d3Hierarchy(rootNode);
    const bubbleLayout = d3
      .pack<NodeData>()
      .size([diameter, diameter])
      .padding(padding);

    const layout = hierarchyNode && bubbleLayout(hierarchyNode);
    setD3Layout(layout);
  }, [domain, datasets]);

  const groupVars = [
    ['Filters', draftExp.filterVariables, 'slategrey'], // => item[0], item[1], item[2]
    ['Variables', draftExp.variables, '#5cb85c'],
    ['Covariates', draftExp.coVariables, '#f0ad4e'],
  ]
    .filter((item) => item[1] && item[1].length)
    .map((item) => ({
      name: item[0] as string,
      items: item[1] as string[],
      color: item[2] as string,
    }));

  if (!d3Layout)
    return (
      <SpinnerContainer>
        <Spinner animation="border" variant="info" />
      </SpinnerContainer>
    );

  return (
    <CirclePack
      layout={d3Layout}
      selectedNode={selectedNode}
      groupVars={groupVars}
      handleSelectNode={handleSelectNode}
    />
  );
};

export default D3Container;
